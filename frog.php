<?php
require_once("animal.php");

class Frog extends Animal{
    public $soundJump = "hop hop";
    public $legs = 4;

    public function __construct($name){
        return $this->name = $name;
    }

    public function jump(){
        return $this->soundJump;
    }
}
?>