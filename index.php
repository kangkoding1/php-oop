<?php
    require("animal.php");
    require("ape.php");
    require("frog.php");

    $sheep = new Animal("shaun");

    echo $sheep->get_name();
    echo "<br>";
    echo $sheep->get_legs();
    echo "<br>";
    echo $sheep->get_cold_blooded();
    echo "<br>";

    $sungokong = new Ape("kera sakti");
    echo $sungokong->yell(); // "Auooo"
    echo "<br>";
    echo $sungokong->get_legs();
    echo "<br>";

    $kodok = new Frog("buduk");
    echo $kodok->jump(); // "hop hop"
    echo "<br>";
    echo $kodok->get_legs();
    echo "<br>";
    
    
?>
