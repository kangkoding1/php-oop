<?php
require_once("animal.php");

class Ape extends Animal{
    public $soundYell = "Auooo";

    public function __construct($name){
        return $this->name = $name;
    }

    public function yell(){
        return $this->soundYell;
    }
}
?>